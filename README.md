# TBG Coding Exercise

## Exercise Instructions

    Write a program that creates a CSV report based on the provided log file.

    The program should count the number of requests made by the IP addresses
    contained in the log file.  The report should only count GET requests made
    over the standard port used for HTTP and should exclude from the count all
    requests made from IP's beginning with '207.114'.  The first field of the
    report should contain the number of requests and the second field should
    contain the IP address that made them.  The report should be ordered so
    that IPs that made the most requests are listed first.  IPs that made the
    same number of requests should be ordered amongst themselves with the IP
    octets of greater values listed first.

    Use the general purpose programming language in which you are most
    proficient to solve the problem.

    In your response, please provide the program source code, the CSV file
    created by the program and how long it took for you to solve the problem.

    NOTE: This screening exercise isn't representative of the type of work
    developers usually do at TBG.  It's a simple test of programming ability.

    EXAMPLE: Given the input:

    2010-08-12 00:00:01 69.143.116.98 - W3SVC106 STREAM 207.22.66.152 80 GET /includes/scripts.js - 200 0 2258 381 94 HTTP/1.1 www.mercymed.com Mozilla/4.0+(compatible;+MSIE+7.0;+Windows+NT+6.0;+WOW64;+GoogleT5;+SLCC1;+.NET+CLR+2.0.50727;+Media+Center+PC+5.0;+.NET+CLR+3.5.30729;+.NET+CLR+3.0.30618;+.NET4.0C) - http://www.mercymed.com/
    2010-08-12 00:00:01 69.143.116.98 - W3SVC106 STREAM 207.22.66.152 80 GET /p7pm/p7popmenu.js - 200 0 7700 379 188 HTTP/1.1 www.mercymed.com Mozilla/4.0+(compatible;+MSIE+7.0;+Windows+NT+6.0;+WOW64;+GoogleT5;+SLCC1;+.NET+CLR+2.0.50727;+Media+Center+PC+5.0;+.NET+CLR+3.5.30729;+.NET+CLR+3.0.30618;+.NET4.0C) - http://www.mercymed.com/
    ...

    The program should output a file called 'report.csv' containing the following:

    8, "69.143.116.98"
    3, "65.37.53.228"
    1, "169.123.16.100"
    ...

## Testing Solution

### Option A
1. Open the `TBGCodingExercise.sln` solution file in the `src` directory.
2. Place an `access.log` file inside of the `src\TBGCodingExercise\Input\` directory.
3. *Build and run* the solution. The resulting report CSV will be located in `src\TBGCodingExercise\Output`.

### Option B
1. Open the `TBGCodingExercise.sln` solution file in the `src` directory and build the solution in release mode.
2. Open a new command prompt window and navigate to the `src\TBGCodingExercise\bin\Release\netcoreapp3.1` folder.
3. Run the following example command: ```TBGCodingExercise.exe --input "C:\Users\YOURUSER\Desktop\access.log" --output "C:\Users\YOURUSER\Desktop\report.csv" --verbose```
   - Parameters: 
     - --input or --i (Path to the input W3C log file. Default: `src\TBGCodingExercise\Input\access.log`)
     - --output or --o (Path to the output CSV report file. Default: `src\TBGCodingExercise\Output\report.csv`)
     - --verbose or --v (Output contents of CSV report into the console. Default: `false`)