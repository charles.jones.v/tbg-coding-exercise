﻿using System;
using System.IO;
using System.Linq;
using W3CParser.Parser;
using System.Text;
using CommandLine;

namespace TBGCodingExercise
{
    internal class Program
    {
        private static string _inputLogFilePath = "../../../../Input/access.log";
        private static string _outputReportFilePath = "../../../../Output/report.csv";
        private static bool _verboseOutput;

        private static void Main(string[] args)
        {
            // Parse optional command line arguments
            Parser.Default.ParseArguments<CommandLineArgs>(args).WithParsed(o =>
            {
                if (!string.IsNullOrEmpty(o.Input))
                {
                    _inputLogFilePath = o.Input;
                }
                if (!string.IsNullOrEmpty(o.Output))
                {
                    _outputReportFilePath = o.Output;
                }
                if (o.Verbose)
                {
                    _verboseOutput = o.Verbose;
                }
            });

            // Check if input file exists
            if (!File.Exists(_inputLogFilePath)) {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"Log file not found at {Path.GetFullPath(_inputLogFilePath)}", Console.ForegroundColor);
                Console.ForegroundColor = ConsoleColor.White;
                return;
            }

            // Open log file contents for reading
            var stringReader = File.OpenText(_inputLogFilePath);

            // Parse log file contents using the W3CReader helper
            var reader = new W3CReader(stringReader);
            var events = reader.Read().Where(w => w.Method != null);

            // Filter events according to specifications
            var filteredEvents = events.Where(w => !w.ClientIpAddress.StartsWith("207.114.") && w.Method.Equals("GET") && w.Port.Equals(80));

            // Group events according to specifications
            var groupedEvents = filteredEvents.GroupBy(w => w.ClientIpAddress).Select(g => new { g.Key, Count = g.Count() });

            // Order events based on specifications
            var orderedEvents = groupedEvents.OrderByDescending(g => g.Count).ThenByDescending(g => g.Key);

            // Generate contents of CSV reports file
            var csvContents = new StringBuilder("Request Count,IP Address\n");
            foreach (var entry in orderedEvents)
            {
                var rowText = $"{entry.Count},\"{entry.Key}\"";
                csvContents.AppendLine(rowText);
                if (_verboseOutput) Console.WriteLine(rowText);
            }

            // Write CSV reports file
            File.WriteAllText(_outputReportFilePath, csvContents.ToString());
        }
    }

    public class CommandLineArgs
    {
        [Option('i', "input", Required = false, HelpText = "Path to the input W3C log file.")]
        public string Input { get; set; }

        [Option('o', "output", Required = false, HelpText = "Path to the output CSV report file.")]
        public string Output { get; set; }

        [Option('v', "verbose", Required = false, HelpText = "Output contents of CSV report into the console.")]
        public bool Verbose { get; set; }
    }
}
